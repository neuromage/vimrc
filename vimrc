""" vim:fdm=marker

filetype plugin indent off

" Go code goodness.
set runtimepath+=/usr/local/go/misc/vim

" Personal scripts.
set rtp+=~/Code/vimscripts/mygrep.vim
set rtp+=~/Code/vimscripts/fix-whitespace.vim

source $VIMRUNTIME/macros/matchit.vim

filetype plugin indent on

call plug#begin('~/.vim/plugged')


" For better JS syntax coloring.
Plug 'pangloss/vim-javascript'

Plug 'vim-scripts/bufkill.vim'
Plug 'Rename'
Plug 'tpope/vim-commentary'
Plug 'plasticboy/vim-markdown'

Plug 'Blackrush/vim-gocode'

augroup JSFiles
  au!
  au FileType javascript setlocal cino+=(0
  au FileType javascript setlocal indentkeys=0{,0},0#,!^F,o,O,e
augroup END


function! GoFmt()
    try
        exe "undojoin"
        exe "Fmt"
    catch
    endtry
endfunction

augroup GoCode
  au!
  autocmd FileType go autocmd BufWritePre <buffer> call GoFmt()
augroup END

" Haskell
" Plug 'lukerandall/haskellmode-vim'
" Plug 'eagletmt/ghcmod-vim'
" Plug 'eagletmt/neco-ghc'
" Plug 'Shougo/vimproc.vim'
" Plug 'vim-scripts/haskell.vim'
" Plug 'Shougo/vimshell.vim'

" let g:haddock_browser='open'
" let g:haddock_browser_callformat='%s %s'
" augroup HaskellMode
"   au!
"   au BufEnter,BufWinEnter *.hs compiler ghc
" augroup END


" Color schemes
Plug 'sickill/vim-monokai'
Plug 'jonathanfilip/vim-lucius'
Plug 'CSApprox'
Plug 'jellybeans.vim'
Plug 'xoria256.vim'
Plug 'noahfrederick/Hemisu'
Plug 'altercation/vim-colors-solarized'
Plug 'sjl/badwolf'
Plug 'Lokaltog/vim-distinguished'
Plug 'Slava/vim-colors-tomorrow'
Plug 'junegunn/seoul256.vim'


" Quick reload and editing of this file
nnoremap <Leader>vs :source $MYVIMRC<CR>
nnoremap <Leader>ve :call EditVimRC()<CR>

function! EditVimRC()
  tabnew $MYVIMRC
endfunction

" Auto source vimrc when written
augroup myvimrchooks
    au!
    autocmd! bufwritepost vimrc source ~/.vimrc
    " Fix a bug in Powerline reload.
    " autocmd bufwritepost vimrc call Pl#Load()
augroup END

syntax on

" My mapleader
let mapleader = "\\"

set cmdheight=3

" Let backspace work over everything.
set backspace=indent,eol,start

set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2

" Line numbers.
set number
set ruler

" Remember last 200 commands
set history=200
set undolevels=1000

" Scrolling
set scrolloff=5

"Formatoptions
set formatoptions+=j

" Don't beep. See http://vim.wikia.com/wiki/Disable_beeping
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Useful mappings {{{
nnoremap <C-s> :w<CR>

" Filetype autocommands {{{
augroup Markdown
  au!
  au BufWinEnter *.md set filetype=markdown
augroup END

augroup AutoClean
  au!
  au BufWritePost * RemoveWhitespace
augroup END

" Indentation
set autoindent
set smartindent

" Reindent within context.
nnoremap <Leader>i mp=ap`p

" Add semicolon to end of file.
function! AddSemicolon()
  execute "normal! mAA;\e`A"
endfunction
nnoremap <Leader>; :call AddSemicolon()<CR>

" Buffers {{{
set hidden
set splitbelow

nnoremap <M-j> <C-w>j
nnoremap <M-k> <C-w>k
nnoremap <M-l> <C-w>l
nnoremap <M-h> <C-w>h

nnoremap <Leader>q :close<CR>
nnoremap <Leader>O :only<CR>

" Tabs
" nnoremap <M-j> :tabp <Enter>
" inoremap <M-j> <Esc>:tabp <Enter>

" nnoremap <M-k> :tabn <Enter>
" inoremap <M-k> <Esc>:tabn <Enter>

" nnoremap <M-t> :tabnew <Enter>
" inoremap <M-t> <Esc>:tabnew <Enter>

" nnoremap <M-w> :tabc <Enter>
" inoremap <M-w> <Esc>:tabc <Enter>

" Searching {{{
set ignorecase
set smartcase
set incsearch
set wrapscan

" Search highlighting
set nohls
"Toggle on search highlighting if needed
nnoremap <silent> <Leader>h :set hls! <CR>

" More useful space bar.
nnoremap <Space> /

" Get highlighted text into LHS for substitute command.
" Use register v for this operation.
nnoremap <Leader>vr :%s/<C-r>=expand("<cword>")<CR>/
vnoremap <Leader>vr "vy:<C-U>%s/<C-r>v/
" }}}

" Ex mode completion {{{
set wildmenu
set wildmode=list:longest,full

" Ignore files of these types.
set wildignore=*.o,*.bak,*.aux,*.log,*.bbl,*.blg,*.pdf,*.ps,/*.git*/,/*.swp*/

" Completion options.
set completeopt=menu,preview

" Quickfix buffer
nnoremap <M-n> :cnext <Enter>
nnoremap <M-p> :cprev <Enter>
nnoremap <silent> <M-o> :QFix <Enter>

let g:quickfixwindow = 0
au! BufReadPost quickfix let g:quickfixwindow = 1
au! BufHidden quickfix let g:quickfixwindow = 0

function! QuickFixOpen()
  botright copen 10
  let g:quickfixwindow = 1
endfunction

function! QuickFixClose()
  cclose
  let g:quickfixwindow = 0
endfunction

function! QuickFixToggle()
  if g:quickfixwindow
    call QuickFixClose()
  else
    call QuickFixOpen()
  endif
endfunction
command! QFix call QuickFixToggle()

" UI {{{

" Fonts.
if has("gui")
  if has("macunix")
    set guifont=monofur:h15
    set macmeta
  else
    set guifont=monofur\ 12
  endif
  set guioptions=
endif

" Colours.
set bg=dark
if !has("gui_running")
  set t_Co=256
  set mouse=a
  set ttymouse=xterm2
endif
highlight ColorColumn guibg=Black ctermbg=Black


"Highlight columns 81-83
set colorcolumn=+1,+2

" Cursor line.
augroup cline
    au!
    au WinLeave * set nocursorline
    au WinEnter * set cursorline
    au InsertEnter * set nocursorline
    au InsertLeave * set cursorline
augroup END


" Status line stuff
set laststatus=2
set encoding=utf-8

" Visual mode reselect
vnoremap < <gv
vnoremap > >gv

" Terminal Vim {{{
" Hack to get Alt keybindings to work in terminal vim.
let c='a'
while c <= 'z'
  exec "set <A-".c.">=\e".c
  exec "imap \e".c." <A-".c.">"
  let c = nr2char(1+char2nr(c))
endw

set timeout ttimeoutlen=50

" C++
function! SplitHeader()
  let l:filetype = &filetype
  if (l:filetype !=? 'cpp')
    echohl ModeMsg
    echom "Not a CPP file"
    echohl None
    return
  endif

  execute 'leftabove 20sp +/#include'
endfunction

command! SplitHeader call SplitHeader()
nnoremap <Leader>h :SplitHeader<CR>

" Powerline {{{
Plug 'Lokaltog/vim-powerline'

let g:Powerline_symbols = 'unicode'
let g:Powerline_stl_path_style='filename'

" Gundo {{{
Plug 'sjl/gundo.vim'

nnoremap <Leader>u :GundoToggle<CR>
let g:gundo_preview_bottom = 1
let g:gundo_width = 35
let g:gundo_close_on_revert = 1

" Fugitive {{{
Plug 'tpope/vim-fugitive'

" Auto delete fugitive buffers when I leave them
autocmd BufReadPost fugitive://* set bufhidden=delete

" Ctrl-P {{{
Plug 'kien/ctrlp.vim'

set rtp+=~/Code/vimscripts/piper-ctrlp.vim

nnoremap <silent> <C-p> :CtrlP<CR>
if has("gui_running")
  nnoremap <silent> <C-Space> :CtrlPBuffer<CR>
else
  nnoremap <silent> <Nul> :CtrlPBuffer<CR>
endif

let g:ctrlp_match_window_reversed = 1
let g:ctrlp_max_height = 15
let g:ctrlp_switch_buffer = 0
let g:ctrlp_by_filename = 0
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:15'
let g:ctrlp_extensions = ['piper', 'line']

let g:ctrlp_abbrev = {
      \ 'gmode' : 't',
      \ 'abbrevs': [
      \ { 'pattern': '^cqjs',
      \   'expanded': 'crawler/quality/ui_server/static/js/',
      \   'mode': 'pfrz',
      \ },
      \ {
        \ 'pattern': '^cd v',
        \ 'expanded': '@cd ~/.vim',
        \ 'mode': 'pfrz',
      \ },
      \ {
        \ 'pattern': '^cd z',
        \ 'expanded': '@cd ~/zsh',
        \ 'mode': 'pfrz',
      \ },
      \ ],
      \}

" }}}

" FSwitch {{{
Plug 'derekwyatt/vim-fswitch'

au! BufEnter *.cc let b:fswitchdst = 'h' | let b:fswitchlocs ='.'
au! BufEnter *.h let b:fswitchdst = 'cc' | let b:fswitchlocs ='.'
nnoremap <Leader>oo :FSHere<CR>
nnoremap <Leader>or :FSSplitRight<CR>
nnoremap <Leader>oR :FSRight<CR>
nnoremap <Leader>ol :FSSplitLeft<CR>
nnoremap <Leader>oL :FSLeft<CR>
" }}}

" Supertab {{{
Plug 'ervandew/supertab'

let g:SuperTabDefaultCompletionType = "<c-n>"
" }}}

" Command Window {{{
set cmdheight=2

cnoremap <C-p> <Up>
cnoremap <C-d> <Down>
" }}}

" Syntastic {{{
" Plug 'scrooloose/syntastic'
Plug 'neuromage/syntastic'
let g:syntastic_python_checker = 'flake8'
" let g:syntastic_python_checker = 'flake8'

" let g:syntastic_go_checkers = ['go', 'gofmt', 'gocode', 'gotype', 'govet']

let g:syntastic_check_on_open=1
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'

nnoremap <Leader>S :SyntasticToggleMode<CR>
nnoremap <Leader>se :Errors<CR>
nnoremap <Leader>ss :SyntasticToggleMode<CR>

" Ultisnips {{{
Plug 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"

let g:UltiSnipsEditSplit='horizontal'
let g:UltiSnipsSnippetsDir='~/.vim/UltiSnips'
let g:UltiSnipsDontReverseSearchPath=1

nnoremap <Leader>os :UltiSnipsEdit<CR>

" IndentLine
Plug 'Yggdroot/indentLine'

let g:indentLine_char = '┊'
let g:indentLine_fileType = ['cpp', 'python', 'javascript', 'vim']

call plug#end()


colorscheme jellybeans
